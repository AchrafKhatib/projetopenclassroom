import { Component, OnInit, OnDestroy } from "@angular/core";
import { Observable, Subscription } from "rxjs";
import "rxjs/Rx";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit, OnDestroy {
  second: number;
  counterSubscription: Subscription;
  constructor() {}

  ngOnInit() {
    const counter = Observable.interval(1000);
    this.counterSubscription = counter.subscribe(
      value => {
        this.second = value;
      },
      err => {
        console.log(err);
      },
      () => {
        console.log("complete");
      }
    );
  }
  ngOnDestroy() {
    this.counterSubscription.unsubscribe();
  }
}
