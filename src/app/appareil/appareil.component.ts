import { Component, OnInit, Input } from "@angular/core";
import { AppareilService } from "../services/appareil.service";

@Component({
  selector: "app-appareil",
  templateUrl: "./appareil.component.html",
  styleUrls: ["./appareil.component.css"]
})
export class AppareilComponent implements OnInit {
  @Input() appareilName: string;
  @Input() active: boolean;
  @Input() indexOfAppareil: number;
  @Input() id: number;

  constructor(private appareilService: AppareilService) {}

  ngOnInit() {}

  getColor() {
    if (this.active) {
      return "Green";
    } else {
      return "red";
    }
  }
  onSwitchOn() {
    this.appareilService.oneOnActive(this.indexOfAppareil);
  }
  onSwitchOff() {
    this.appareilService.oneOffActive(this.indexOfAppareil);
  }
}
