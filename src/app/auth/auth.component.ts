import { Component, OnInit } from "@angular/core";
import { AuthService } from "../services/auth.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-auth",
  templateUrl: "./auth.component.html",
  styleUrls: ["./auth.component.css"]
})
export class AuthComponent implements OnInit {
  authStatus: boolean;

  constructor(private autService: AuthService, private router: Router) {}

  ngOnInit() {
    this.authStatus = this.autService.isAuth;
  }

  onSignIn() {
    this.autService.signIn().then(() => {
      this.router.navigate(["appareils"]);
      this.authStatus = this.autService.isAuth;
    });
  }

  onSignOut() {
    this.autService.signOut();
    this.authStatus = this.autService.isAuth;
  }
}
