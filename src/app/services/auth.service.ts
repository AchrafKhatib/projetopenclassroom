import { promise } from "protractor";
import { resolve, reject } from "q";

export class AuthService {
  isAuth: boolean = false;

  signIn() {
    return new Promise((resolve, reject) =>
      setTimeout(() => {
        this.isAuth = true;
        resolve(true);
      }, 1000)
    );
  }
  signOut() {
    this.isAuth = false;
  }
}
