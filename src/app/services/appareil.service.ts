import { Subject } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable()
export class AppareilService {
  appareilSubject = new Subject<any[]>();

  constructor(private httpClient: HttpClient) {}

  private appareils = [];

  emitAppareilSubject() {
    this.appareilSubject.next(this.appareils.slice());
  }
  getAppareilId(id: number) {
    const appareil = this.appareils.find(objet => {
      return objet.id === id;
    });
    return appareil;
  }
  switchOnAll() {
    for (let appareil of this.appareils) {
      appareil.active = true;
    }
    this.emitAppareilSubject();
  }
  switchOffAll() {
    for (let appareil of this.appareils) {
      appareil.active = false;
    }
    this.emitAppareilSubject();
  }
  oneOnActive(index: number) {
    this.appareils[index].active = true;
    this.emitAppareilSubject();
  }
  oneOffActive(index: number) {
    this.appareils[index].active = false;
    this.emitAppareilSubject();
  }
  addAppareil(name: string, active: boolean) {
    const appareilObject = {
      id: 0,
      name: "",
      active: false
    };
    appareilObject.name = name;
    appareilObject.active = active;
    appareilObject.id = this.appareils[this.appareils.length - 1].id + 1;
    this.appareils.push(appareilObject);
    this.emitAppareilSubject();
  }

  saveAppareilToServeur() {
    this.httpClient
      .put(
        "https://http-client-demo-bbda6.firebaseio.com/appareils.json",
        this.appareils
      )
      .subscribe(() => {
        console.log("enregistrement terminée"),
          err => {
            console.log("error detecter" + err);
          };
      });
  }
  getAppareilToServeur() {
    this.httpClient
      .get<any[]>(
        "https://http-client-demo-bbda6.firebaseio.com/appareils.json"
      )
      .subscribe(
        response => {
          this.appareils = response;
          this.emitAppareilSubject();
        },
        err => {
          console.log("error" + err);
        }
      );
  }
}
