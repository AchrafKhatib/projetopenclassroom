import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { AppareilService } from "../services/appareil.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-edit-appareil",
  templateUrl: "./edit-appareil.component.html",
  styleUrls: ["./edit-appareil.component.css"]
})
export class EditAppareilComponent implements OnInit {
  default: boolean = false;
  constructor(
    private appareilService: AppareilService,
    private router: Router
  ) {}

  ngOnInit() {}

  onSubmit(form: NgForm) {
    const name = form.value["name"];
    const active = form.value["active"];
    this.appareilService.addAppareil(name, active);
    this.router.navigate(["/appareils"]);
  }
}
